#version 430 core

layout (location = 0) in vec4 vPosition;
layout (location = 1) in vec4 vColor;

out vec4 color;
uniform int time;

void main()
{
   color = vColor;
   vec4 newPos = vPosition;
   newPos.x = newPos.x * cos(time * 3.141592 / 180.0);
   gl_Position = newPos;
}